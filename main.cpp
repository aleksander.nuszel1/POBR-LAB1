#include <iostream>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

uchar truncate(int a) {
    if (a<0) {
        return 0;
    }
    if (a > 255) {
        return 255;
    }
    return a;
}

cv::Vec3b makeGrey(cv::Vec3b &color) {
    uchar grey = (color[0] + color[1] + color[2])/3;
    color[0] = truncate(color[2] - grey);
    color[1] = truncate(color[2] - grey);
    color[2] = truncate(color[2] - grey);
    return color;
}

cv::Vec3b changeContrast(cv::Vec3b &color, float x) {
    color[0] = truncate(color[0] * x);
    color[1] = truncate(color[1] * x);
    color[2] = truncate(color[2] * x);
    return color;
}

cv::Vec3b changeBrightness(cv::Vec3b &color, uchar y) {
    color[0] = truncate(color[0] + y);
    color[1] = truncate(color[1] + y);
    color[2] = truncate(color[2] + y);
    return color;
}

void changeImage(cv::Mat &image, float x, int y) {

    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            if (i - j > 0 && i + j < image.rows) {
                image.at<cv::Vec3b>(cv::Point(i, j)) = changeContrast(image.at<cv::Vec3b>(cv::Point(i, j)), x);
            }
            /**
             * NOP, compiler will optimize it
             */
            if (i - j > 0 && i + j > image.rows) {
                ;
            }

            if (i - j < 0 && i + j >= image.rows) {
                image.at<cv::Vec3b>(cv::Point(i, j)) = changeBrightness(image.at<cv::Vec3b>(cv::Point(i, j)), y);
            }

            if (i - j < 0 && i + j < image.rows) {
                image.at<cv::Vec3b>(cv::Point(i, j)) = makeGrey(image.at<cv::Vec3b>(cv::Point(i, j)));
            }
        }
    }
}

int pixelBrightnessIndex(cv::Vec3b &color) {
    int brightness = (color[0] + color[1] + color[2])/3;
    return brightness/32;
}

void printHist(cv::Mat &image) {
    const int buckets = 8;
    int histValues[]= {0,0,0,0,0,0,0,0};
    for (int i = 0; i < image.rows; ++i) {
        for (int j = 0; j < image.cols; ++j) {
            cv::Vec3b color = image.at<cv::Vec3b>(cv::Point(i, j));
            histValues[pixelBrightnessIndex(color)] += 1;
        }
    }
    int sum = 0;
    for (int i=0; i < buckets; i++) {
        sum += histValues[i];
        std::cout<<"Pikseli o jasności " << i*32<<"-"<<(i+1)*32-1<<" jest "<< histValues[i]<<std::endl;
    }
    std::cout<<"Suma pikseli "<<sum<<std::endl;
}


int main(int, char *[]) {
    float x = 1.4;
    int y = 70;
    cv::Mat image = cv::imread("lena.png");
    printHist(image);
    changeImage(image, x, y);
    cv::imshow("Lena", image);
    cv::imwrite("lena_produced.png", image);
    cv::waitKey(-1);
    return 0;
}
